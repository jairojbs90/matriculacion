# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'template.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainTemplate(object):
    def setupUi(self, MainTemplate):
        if not MainTemplate.objectName():
            MainTemplate.setObjectName(u"MainTemplate")
        MainTemplate.resize(800, 600)
        self.actionEstudiantes = QAction(MainTemplate)
        self.actionEstudiantes.setObjectName(u"actionEstudiantes")
        self.actionCursos = QAction(MainTemplate)
        self.actionCursos.setObjectName(u"actionCursos")
        self.actionMatricula = QAction(MainTemplate)
        self.actionMatricula.setObjectName(u"actionMatricula")
        self.actionSalir = QAction(MainTemplate)
        self.actionSalir.setObjectName(u"actionSalir")
        self.centralwidget = QWidget(MainTemplate)
        self.centralwidget.setObjectName(u"centralwidget")
        MainTemplate.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainTemplate)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 21))
        self.menuMatriculacion = QMenu(self.menubar)
        self.menuMatriculacion.setObjectName(u"menuMatriculacion")
        self.menuIngreso = QMenu(self.menuMatriculacion)
        self.menuIngreso.setObjectName(u"menuIngreso")
        self.menuArchivo = QMenu(self.menubar)
        self.menuArchivo.setObjectName(u"menuArchivo")
        MainTemplate.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainTemplate)
        self.statusbar.setObjectName(u"statusbar")
        MainTemplate.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuArchivo.menuAction())
        self.menubar.addAction(self.menuMatriculacion.menuAction())
        self.menuMatriculacion.addAction(self.menuIngreso.menuAction())
        self.menuMatriculacion.addAction(self.actionMatricula)
        self.menuIngreso.addAction(self.actionEstudiantes)
        self.menuIngreso.addAction(self.actionCursos)
        self.menuArchivo.addAction(self.actionSalir)

        self.retranslateUi(MainTemplate)

        QMetaObject.connectSlotsByName(MainTemplate)
    # setupUi

    def retranslateUi(self, MainTemplate):
        MainTemplate.setWindowTitle(QCoreApplication.translate("MainTemplate", u"Unidad Educativa", None))
        self.actionEstudiantes.setText(QCoreApplication.translate("MainTemplate", u"Estudiantes", None))
        self.actionCursos.setText(QCoreApplication.translate("MainTemplate", u"Cursos", None))
        self.actionMatricula.setText(QCoreApplication.translate("MainTemplate", u"Matricula", None))
        self.actionSalir.setText(QCoreApplication.translate("MainTemplate", u"Salir", None))
        self.menuMatriculacion.setTitle(QCoreApplication.translate("MainTemplate", u"Matriculacion", None))
        self.menuIngreso.setTitle(QCoreApplication.translate("MainTemplate", u"Ingreso", None))
        self.menuArchivo.setTitle(QCoreApplication.translate("MainTemplate", u"Archivo", None))
    # retranslateUi


# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'login.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_FrmLogin(object):
    def setupUi(self, FrmLogin):
        if not FrmLogin.objectName():
            FrmLogin.setObjectName(u"FrmLogin")
        FrmLogin.resize(271, 136)
        self.lbUsuario = QLabel(FrmLogin)
        self.lbUsuario.setObjectName(u"lbUsuario")
        self.lbUsuario.setGeometry(QRect(50, 30, 47, 13))
        self.lblClave = QLabel(FrmLogin)
        self.lblClave.setObjectName(u"lblClave")
        self.lblClave.setGeometry(QRect(50, 60, 47, 13))
        self.txtUsuario = QLineEdit(FrmLogin)
        self.txtUsuario.setObjectName(u"txtUsuario")
        self.txtUsuario.setGeometry(QRect(100, 30, 113, 20))
        self.txtClave = QLineEdit(FrmLogin)
        self.txtClave.setObjectName(u"txtClave")
        self.txtClave.setGeometry(QRect(100, 60, 113, 20))
        self.btnIngresar = QPushButton(FrmLogin)
        self.btnIngresar.setObjectName(u"btnIngresar")
        self.btnIngresar.setGeometry(QRect(140, 90, 75, 23))

        self.retranslateUi(FrmLogin)

        QMetaObject.connectSlotsByName(FrmLogin)
    # setupUi

    def retranslateUi(self, FrmLogin):
        FrmLogin.setWindowTitle(QCoreApplication.translate("FrmLogin", u"Login", None))
        self.lbUsuario.setText(QCoreApplication.translate("FrmLogin", u"Usuario", None))
        self.lblClave.setText(QCoreApplication.translate("FrmLogin", u"Clave", None))
        self.btnIngresar.setText(QCoreApplication.translate("FrmLogin", u"Ingresar", None))
    # retranslateUi

